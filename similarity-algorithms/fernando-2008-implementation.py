from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from gensim import corpora, models
import gensim
import nltk
import os
import numpy as np
import re
import glob
from sets import Set


stopset = set(stopwords.words('english'))
stemmer = nltk.PorterStemmer()

#method returns the maximum similarity for given two words from knowledge base similarity measures
def getKnowledgeBasedSimilarity(worda, wordb):
	wordasynsets = wn.synsets(worda[0], worda[1].lower())
	wordbsynsets = wn.synsets(wordb[0], wordb[1].lower())
	synsetnamea = [wn.synset(str(syns.name())) for syns in wordasynsets]
	synsetnameb = [wn.synset(str(syns.name())) for syns in wordbsynsets]
	brown_ic = wordnet_ic.ic('ic-brown.dat')
	
        jcnmax = 1e+300
        similarities = []
        for sseta, ssetb in [(sseta,ssetb) for sseta in synsetnamea for ssetb in synsetnameb]:
		try:
			jcnsim = sseta.jcn_similarity(ssetb, brown_ic)
			similarities.append(jcnsim/jcnmax)
		except nltk.corpus.reader.wordnet.WordNetError:
			print "no similarity for "+str(sseta), str(ssetb)
	if not similarities:
		return 0 # when at least one input word does not have given pos tag in wordnet (eg: difficult is adj only in wordnet, but can be 'n' in sentences)
	else:
		sim=max(similarities)
		if sim > 1:
			sim = 1
		elif sim < 0.6:
			sim = 0
		return sim

def get_wordnet_pos(treebank_tag):
	if treebank_tag.startswith('J'): 
		return wn.ADJ
	elif treebank_tag.startswith('V'): 
		return wn.VERB
	elif treebank_tag.startswith('N'):
		return wn.NOUN
	elif treebank_tag.startswith('R'):
		return wn.ADV
	else: 
		return ''

def getSimilarity(sentence1, sentence2):
	tokens1 = word_tokenize(sentence1)
	tokens2 = word_tokenize(sentence2)
	pos_tag1 = nltk.pos_tag(tokens1)
        pos_tag2 = nltk.pos_tag(tokens2)

	sentencepos=dict()
	for pos in pos_tag1:
		sentencepos[pos[0]]=(pos[0], get_wordnet_pos(pos[1]))
	for pos in pos_tag2:
		sentencepos[pos[0]]=(pos[0], get_wordnet_pos(pos[1]))

	tokensinvector=Set()
	for token in tokens1:
		tokensinvector.add(token)
	for token in tokens2:
		tokensinvector.add(token)

	sentence1str=''
	sentence2str=''
	for token in tokensinvector:
		if token in tokens1:
			sentence1str+='1 '
		else:
			sentence1str+='0 '

		if token in tokens2:
			sentence2str+='1 '
		else:
			sentence2str+='0 '

	sentence1=np.matrix(sentence1str)
	sentence2=np.matrix(sentence2str)

	similarities=''
	for token1 in tokensinvector:
		worda=sentencepos[token1]
		for token2 in tokensinvector:
			wordb=sentencepos[token2]
			similarities+=str(getKnowledgeBasedSimilarity(worda, wordb))+' '
		similarities+=';'

	similarities=np.matrix(similarities[:-1])
			
	similarity = sentence1*similarities
	sentence2=sentence2.T
	similarity = similarity*sentence2
	
	return similarity[(0, 0)]


if __name__ == "__main__":
	data_folder='def_neighborhood/conditions-neighborhood'
	files = glob.glob('../data-preparation/prepared_data/'+data_folder+'/*-candidates')

	for f in files:
		cui=f[f.rindex('/')+1:f.rindex('-')]
		print "Processing: "+cui
		sentences=open(f).readlines()
		similarity=[]
		results=''
		for sentence in sentences:
			if sentence=='\n':
				results += str(max(similarity))+"\n"
				similarity=[]
			else:
				definition=sentence.split(':')[0].replace(',', ' ')
				candidate=sentence.split(':')[1].strip().replace(',', ' ')
				simVal = getSimilarity(candidate, definition)
				similarity.append(simVal)
				
		f = open('../data-preparation/fernando-results-our-format/'+cui+'-results','w')
	        f.write(results)
       		f.close()
