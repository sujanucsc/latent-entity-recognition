from __future__ import division
from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
from gensim import corpora, models
import gensim
import nltk
import numpy as np
from sets import Set
from nltk.corpus import stopwords
from nltk.tokenize import word_tokenize
import glob
import datetime
from negex import *

stopset=set(stopwords.words('english'))
stemmer=nltk.PorterStemmer()
word2vecmodel = gensim.models.Word2Vec.load_word2vec_format('GoogleNews-vectors-negative300.bin', binary=True)

def levenshtein(a,b):
    "Calculates the Levenshtein distance between a and b."
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a,b = b,a
        n,m = m,n

    current = range(n+1)
    for i in range(1,m+1):
        previous, current = current, [i]+[0]*n
        for j in range(1,n+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if a[j-1] != b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)

    return current[n]

def getWordForm(word):
        synset = wn.synsets(word)
        resultLemmas = []
        result = []
	flagNoun = 0
	flagVerb = 0
        for syn in synset:
		pos=syn.pos()
                for lemma in syn.lemmas():
			lemmaname=lemma.name()
			if((wn.morphy(word) == lemmaname or lemmaname == word) and pos == 'n'): #if given word is noun or verb we dont need to do anything
				result.append(word+".n")
				return result
			elif(pos == 'v'):
				resultLemmas.append(lemma.derivationally_related_forms())
			elif(lemmaname == word and (pos == 'a' or pos == 's')):
				resultLemmas.append(lemma.derivationally_related_forms())
			elif(lemmaname == word and pos == 'r'):
				resultLemmas.append(adverbLemma for adverbLemmaList in getNounForAdverb(lemma) for adverbLemma in adverbLemmaList)

	for lemmas in resultLemmas:
                for lemma in lemmas:
               	        result.append(lemma.name()+"."+lemma.synset().pos())
	return result

#this method will return the noun of the given word
def getWordNounForm(word):
	resultSet = getWordForm(word)
	if not resultSet:#we cant find noun form for some word e.g., chronic, her
		return ''

#we perform two iteration to find the noun form, since some words return other forms than noun when called to get derivationally_related_form
#e.g., inflammatory (this return inflame, and inflame is verb, so we call once more to get the noun
	containsNoun = False
	for result in resultSet:
		if result.split('.')[1] == 'n':
			containsNoun = True

	if containsNoun:
		return getMostSimilarNoun(word, resultSet)
	else:
		resultSecondIteration = []
		for result in resultSet:
			for item in getWordForm(result.split('.')[0]):
				if item.split('.')[1] == 'n':
					resultSecondIteration.append(item)
	return getMostSimilarNoun(word, resultSecondIteration)

#resultsSet typically contain more than one term, we get the best term by calculating the levenshtein distance among the word and each term
def getMostSimilarNoun(word, resultSet):
	if not resultSet:
		return ''
	levenshteinDistance = []
	for result in resultSet:
		levenshteinDistance.append(levenshtein(result.split('.')[0], word))
	
	return resultSet[levenshteinDistance.index(min(levenshteinDistance))]


def getNounForAdverb(adverbLemma):
	result = []
	for lemma in adverbLemma.pertainyms():
		result.append(lemma.derivationally_related_forms())
	return result

def isAntonym(worda, wordb):
	wordasynsets = wn.synsets(worda[0], worda[1])
	wordbsynsets = wn.synsets(wordb[0], wordb[1])
	isAntonym = 0
	for syn in wordasynsets:
		for lemma in syn.lemmas():
			for antonym in lemma.antonyms():
				lst =[]
				lst.append(antonym.name())	
				lst.append(antonym.synset().pos())
				if antonym.name() == wordb or isSynonym(lst, wordb):
					isAntonym = 1
	for syn in wordbsynsets:
                for lemma in syn.lemmas():
                        for antonym in lemma.antonyms():
				lst =[]
                                lst.append(antonym.name())
                                lst.append(antonym.synset().pos())
				if antonym.name() == worda or isSynonym(lst, worda):
                                	isAntonym = 1
	return isAntonym

def isSynonym(worda, wordb):
	sim=-1
	if worda[1] != 'n' or wordb[1]!='n':
		sim=1 - (float(levenshtein(worda[0], wordb[0]))/max(len(worda[0]), len(wordb[0])))
	else:
		sim=getWordNetSimilarity(worda, wordb)

	if sim > 0.95:
		return 1
	else:
		return 0

def getWordNetSimilarity(worda, wordb):
	wordasynsets = wn.synsets(worda[0], worda[1].lower())
        wordbsynsets = wn.synsets(wordb[0], wordb[1].lower())
        synsetnamea = [wn.synset(str(syns.name())) for syns in wordasynsets]
        synsetnameb = [wn.synset(str(syns.name())) for syns in wordbsynsets]
        brown_ic = wordnet_ic.ic('ic-brown.dat')

        lchmax = 3.6375861597263857
        resmax = 14.4655999131
        jcnmax = 1e+300
        similarities = []
	total_similarity = 1.0
        for sseta, ssetb in [(sseta,ssetb) for sseta in synsetnamea for ssetb in synsetnameb]:
                        wupsim = sseta.wup_similarity(ssetb)
                        lchsim = sseta.lch_similarity(ssetb)
                        ressim = sseta.res_similarity(ssetb, brown_ic)
                        jcnsim = sseta.jcn_similarity(ssetb, brown_ic)
                        linsim = sseta.lin_similarity(ssetb, brown_ic)
                        similarities.append(wupsim)
                        similarities.append(lchsim/lchmax)
                        similarities.append(ressim/resmax)
                        similarities.append(jcnsim/jcnmax)
                        similarities.append(linsim)
	similarities.append(1 - (levenshtein(worda[0], wordb[0])/max(len(worda[0]), len(wordb[0]))))
	try:	
		similarities.append(word2vecmodel.similarity(worda[0], wordb[0]))
	except KeyError:
		similarities.append(0)

        sim = max(similarities)
        if sim > 1:
        	sim = 1
	
	if sim < 0.6:
		return 0
	else:
		return sim	


def getKnowledgeBasedSimilarity(worda, wordb):
	if worda[1]!='n' or wordb[1]!='n':
		return 1 - (float(levenshtein(worda[0], wordb[0]))/max(len(worda[0]), len(wordb[0])))
        
	if isAntonym(worda, wordb):
		return -1

	if isSynonym(worda, wordb):
		return 1

	return getWordNetSimilarity(worda, wordb)


def getNormalizedWeights(cui, entropies, semantic_models):
	defs=open(semantic_models+cui).readlines()
	vals=[]
	for definition in defs:
		comps=definition.strip()[:-1].split(',')
		for com in comps:
			try:
				val=float(entropies[stemmer.stem(com)])
			except KeyError:
				val=2.95604304909
			vals.append(val)
	maxval=max(vals)

	normalizedvalues=dict()
	for definition in defs:
		comps=definition.strip()[:-1].split(',')
		for comp in comps:
			try:
				val=float(entropies[stemmer.stem(comp)])/maxval
			except KeyError:
				val=0
			normalizedvalues[comp]=val

	return normalizedvalues


print datetime.datetime.now()
simWordNet = []
semantic_models='../data-preparation/semantic-models-original/'
data_folder='def_original/conditions-original'
files = glob.glob('../data-preparation/prepared_data/'+data_folder+'/*-candidates')
rfile = open(r'negex_triggers.txt')
irules = sortRules(rfile.readlines())

entropies=dict()
entropyvals=open('../data-preparation/entropy-stemmed').readlines()
for line in entropyvals:
	term=line.split('\t')[0]
	value=float(line.split('\t')[1])
	if value==1000:
		value=2.95604304909 #normalize to maximum entropy value obtained
	entropies[term]=value

for f in files:
        print "Processing :"+f
	cui=f[f.rindex('/')+1:f.rindex('-')]
	normalizedweights=getNormalizedWeights(cui, entropies, semantic_models)

        lines = open(f).readlines()
        originalSentences = open('../data-preparation/prepared_data/def_dependency/conditions-originals/'+cui).readlines()
        foundAntonym = False;
        lineNumber = 0
        results = ''
        for line in lines:
		ori_comps = originalSentences[lineNumber].split('\t')
        	tagger = negTagger(sentence = ori_comps[0], phrases = [ori_comps[5]], rules = irules, negP=False)
	        negFlag = tagger.getNegationFlag()

		if line=='\n':#end of one sentence
			wordnet_sim = max(simWordNet)

                        if foundAntonym : #if there is antonym with any definition, we consider as negative 
                                wordnet_sim = min(simWordNet)
			elif negFlag=='negated':
				wordnet_sim=wordnet_sim-1
                        
                        simWordNet = []
                        foundAntonym = False
                        results = results + str(wordnet_sim)+" : "+ori_comps[1]+"\n"
                        lineNumber += 1
			continue

                synonymSimilarity = 0
                comps = line.split(':')

		definition=[]
		weightvalues=dict()
		normalizedsum=0
		for comp in comps[0].split(','):
			noun=getWordNounForm(comp.lower().strip())
			if noun!='':
				definition.append(noun)
				weightvalues[noun.split('.')[0]]=float(normalizedweights[comp])
				normalizedsum+=float(normalizedweights[comp])

                candidate = [appForm for appForm in [getWordNounForm(com.strip()) for com in [comp.lower() for comp in comps[1].split(',')]]]
                candidate[:] = [x for x in candidate if x != '']

                defLength = len(definition)
                for compA, compB in [(compA, compB) for compA in definition for compB in candidate]:
                        compAElements = compA.split(".")
                        compBElements = compB.split(".")
                        if(isSynonym(compAElements, compBElements) and isSynonym(compBElements, compAElements)):#synonyms
                                synonymSimilarity = synonymSimilarity + (1*weightvalues[compAElements[0]])
                                try:
                                        definition.remove(compA)
					candidate.remove(compB)
				except:
					#this happens when candidate has two identical words which are synonyms for word in definition. So it tries to remove the word from definition twice. But second time it is not there in definition. This can happen other way too (definition has two identical words and trying to delete the word from candidate twice)
                                        print 'trying to delete word not in definition or candidate'
                similarity = 0
                for compA in definition:
                        values = [0]
                        compAElements=compA.split(".")
                        for compB in candidate:
                                compBElements = compB.split(".")
                                sim = getKnowledgeBasedSimilarity(compAElements, compBElements)
                                if sim == -1:
					if negFlag=='negated':#becomes a synonym
						values.append(1*weightvalues[compAElements[0]])
					else:
						values = []
	                                        values.append(-1*weightvalues[compAElements[0]])
        	                                foundAntonym = True
                	                        break #should not consider this with other terms in candidate
                                else:
                                        values.append(sim*weightvalues[compAElements[0]])
                        similarity = similarity + max(values)
			
		sim=(similarity+synonymSimilarity)/normalizedsum
		simWordNet.append(sim)


        f = open('../data-preparation/prepared_data/'+data_folder+'/'+cui+'-entropy-results','w')
        f.write(results)
        f.close()
print datetime.datetime.now()

