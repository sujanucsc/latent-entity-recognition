from __future__ import division
from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
from gensim import corpora, models
import gensim
import nltk
import numpy as np
import datetime
from sets import Set
from negex import *
import csv

def levenshtein(a,b):
    "Calculates the Levenshtein distance between a and b."
    n, m = len(a), len(b)
    if n > m:
        # Make sure n <= m, to use O(min(n,m)) space
        a,b = b,a
        n,m = m,n

    current = range(n+1)
    for i in range(1,m+1):
        previous, current = current, [i]+[0]*n
        for j in range(1,n+1):
            add, delete = previous[j]+1, current[j-1]+1
            change = previous[j-1]
            if a[j-1] != b[i-1]:
                change = change + 1
            current[j] = min(add, delete, change)

    return current[n]

def getWordForm(word):
        synset = wn.synsets(word)
        resultLemmas = []
        result = []
	flagNoun = 0
	flagVerb = 0
        for syn in synset:
                for lemma in syn.lemmas:
			if((wn.morphy(word) == lemma.name or lemma.name == word) and lemma.synset.pos == 'n'): #if given word is noun or verb we dont need to do anything
				result.append(word+".n")
				return result
			elif(lemma.synset.pos == 'v'):
				resultLemmas.append(lemma.derivationally_related_forms())
			elif(lemma.name == word and (lemma.synset.pos == 'a' or lemma.synset.pos == 's')):
				resultLemmas.append(lemma.derivationally_related_forms())
			elif(lemma.name == word and lemma.synset.pos == 'r'):
				resultLemmas.append(adverbLemma for adverbLemmaList in getNounForAdverb(lemma) for adverbLemma in adverbLemmaList)

	for lemmas in resultLemmas:
                for lemma in lemmas:
               	        result.append(lemma.name+"."+lemma.synset.pos)
	return result

#this method will return the noun of the given word
def getWordNounForm(word):
	resultSet = getWordForm(word)
	if not resultSet:#we cant find noun form for some word e.g., chronic, her
		return ''

#we perform two iteration to find the noun form, since some words return other forms than noun when called to get derivationally_related_form
#e.g., inflammatory (this return inflame, and inflame is verb, so we call once more to get the noun
	containsNoun = False
	for result in resultSet:
		if result.split('.')[1] == 'n':
			containsNoun = True

	if containsNoun:
		return getMostSimilarNoun(word, resultSet)
	else:
		resultSecondIteration = []
		for result in resultSet:
			for item in getWordForm(result.split('.')[0]):
				if item.split('.')[1] == 'n':
					resultSecondIteration.append(item)
	return getMostSimilarNoun(word, resultSecondIteration)

#resultsSet typically contain more than one term, we get the best term by calculating the levenshtein distance among the word and each term
def getMostSimilarNoun(word, resultSet):
	levenshteinDistance = []
	for result in resultSet:
		levenshteinDistance.append(levenshtein(result.split('.')[0], word))
	
	return resultSet[levenshteinDistance.index(min(levenshteinDistance))]


def getNounForAdverb(adverbLemma):
	result = []
	for lemma in adverbLemma.pertainyms():
		result.append(lemma.derivationally_related_forms())
	return result

def isAntonym(worda, wordb):
	wordasynsets = wn.synsets(worda[0], worda[1])
	wordbsynsets = wn.synsets(wordb[0], wordb[1])
	isAntonym = 0
	for syn in wordasynsets:
		for lemma in syn.lemmas:
			for antonym in lemma.antonyms():
				lst =[]
				lst.append(antonym.name)	
				lst.append(antonym.synset.pos)
				if antonym.name == wordb or isSynonym(lst, wordb):
					isAntonym = 1
	for syn in wordbsynsets:
                for lemma in syn.lemmas:
                        for antonym in lemma.antonyms():
				lst =[]
                                lst.append(antonym.name)
                                lst.append(antonym.synset.pos)
				if antonym.name == worda or isSynonym(lst, worda):
                                	isAntonym = 1
	return isAntonym

def isSynonym(worda, wordb):
	if getWordNetSimilarity(worda, wordb) > 0.95:
		return 1

def getWordNetSimilarity(worda, wordb):
	if worda[1] != wordb[1]:
                return 0
	
	wordasynsets = wn.synsets(worda[0], worda[1])
        wordbsynsets = wn.synsets(wordb[0], wordb[1])
	brown_ic = wordnet_ic.ic('ic-brown.dat')

        lchnormalizingval = 3.6375861597263857  #maximum value taken by lch
        similarities = []
        for sseta, ssetb in [(sseta,ssetb) for sseta in wordasynsets for ssetb in wordbsynsets]:
		pathsim = sseta.path_similarity(ssetb)
                wupsim = sseta.wup_similarity(ssetb)
                lchsim = sseta.lch_similarity(ssetb)
                linsim = sseta.lin_similarity(ssetb, brown_ic)
                similarities.append(pathsim)
                similarities.append(wupsim)
                similarities.append(lchsim/lchnormalizingval)
                similarities.append(linsim)
	
	sim = max(similarities)
	if sim < 0.6:
		return 0
	else:
		return sim	


def getKnowledgeBasedSimilarity(worda, wordb):
	if worda[1] != wordb[1]:
		return 0
        
	if isAntonym(worda, wordb):
		return -1

	if isSynonym(worda, wordb):
		return 1

	return getWordNetSimilarity(worda, wordb)


print datetime.datetime.now()
similarityWithLSIMultiplication = []
simWordNet = []
conditions = open('all-conditions').readline().split(',')
rfile = open(r'negex_triggers.txt')
irules = sortRules(rfile.readlines())

for condition in conditions:
	print "Processing :"+condition
	cui = condition.strip().split(':')[0]
	numberofdefinitions = int(condition.strip().split(':')[1])
	lines = open('conditions/'+cui+'/'+cui+'-def-candidate-terms').readlines()
	originalSentences = open('conditions/'+cui+'/'+cui+'-sentences').readlines()
	foundAntonym = False;
	lineNumber = 0
	results = ''
	for line in lines:
		synonymSimilarity = 0
		comps = line.split(':')
		
		definition = [appForm for appForm in [getWordNounForm(com.strip()) for com in [comp.lower() for comp in comps[0].split(',')]]]
		definition[:] = [x for x in definition if x != '']
		candidate = [appForm for appForm in [getWordNounForm(com.strip()) for com in [comp.lower() for comp in comps[1].split(',')]]]
		candidate[:] = [x for x in candidate if x != '']

		defLength = len(definition)
		for compA, compB in [(compA, compB) for compA in definition for compB in candidate]:
			compAElements = compA.split(".")
			compBElements = compB.split(".")
			if(isSynonym(compAElements, compBElements) and isSynonym(compBElements, compAElements)):#synonyms
				synonymSimilarity = synonymSimilarity + 1
				try:
					definition.remove(compA)
					candidate.remove(compB)
				except:
					print 'abc'
		similarity = 0
		for compA in definition:
                	values = [0]
	                compAElements = compA.split(".")
        	        for compB in candidate:
				compBElements = compB.split(".")
				sim = getKnowledgeBasedSimilarity(compAElements, compBElements)
				if sim == -1:#if compA is antonym, the avoid it being compared to other words since it will void the antonym effect
					values = []
					values.append(-1)
					foundAntonym = True
					break #should not consider this with other terms in candidate
				else:
					values.append(sim)
                	similarity = similarity + max(values)

		simWordNet.append((similarity+synonymSimilarity)/defLength)
		if(len(simWordNet) == numberofdefinitions):
			wordnet_sim = max(simWordNet)
			if foundAntonym : #if there is antonym with any definition, we consider as negative 
        	                wordnet_sim = min(simWordNet)

			comps = originalSentences[lineNumber].split('\t')
			tagger = negTagger(sentence = comps[0], phrases = [comps[3]], rules = irules, negP=False)
			negFlag = tagger.getNegationFlag()
			simWordNet = []
			foundAntonym = False
			results = results + str(wordnet_sim)+" : "+negFlag+"\n"
			lineNumber += 1
	f = open('conditions/'+cui+'/results','w')
	f.write(results) 
	f.close()
print datetime.datetime.now()
