from nltk.corpus import wordnet as wn
from nltk.corpus import wordnet_ic
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from gensim import corpora, models
import gensim
import nltk
import os
import numpy as np
import re
import glob

textFileLocation = 'semeval-data-sample/'
stopset = set(stopwords.words('english'))
stemmer = nltk.PorterStemmer()

#method returns the maximum similarity for given two words from knowledge base similarity measures
def getKnowledgeBasedSimilarity(worda, wordb):
	wordasynsets = wn.synsets(worda[0], worda[1].lower())
	wordbsynsets = wn.synsets(wordb[0], wordb[1].lower())
	synsetnamea = [wn.synset(str(syns.name)) for syns in wordasynsets]
	synsetnameb = [wn.synset(str(syns.name)) for syns in wordbsynsets]
	brown_ic = wordnet_ic.ic('ic-brown.dat')
	
	lchmax = 3.6375861597263857
        resmax = 14.4655999131
        jcnmax = 1e+300
        similarities = []
        total_similarity = 1.0
        for sseta, ssetb in [(sseta,ssetb) for sseta in synsetnamea for ssetb in synsetnameb]:
                        wupsim = sseta.wup_similarity(ssetb)
                        lchsim = sseta.lch_similarity(ssetb)
                        ressim = sseta.res_similarity(ssetb, brown_ic)
                        jcnsim = sseta.jcn_similarity(ssetb, brown_ic)
                        linsim = sseta.lin_similarity(ssetb, brown_ic)
                        similarities.append(wupsim)
                        similarities.append(lchsim/lchmax)
                        similarities.append(ressim/resmax)
                        similarities.append(jcnsim/jcnmax)
                        similarities.append(linsim)
	
	if not similarities:
		return 0 # when at least one input word does not have given pos tag in wordnet (eg: difficult is adj only in wordnet, but can be 'n' in sentences)
	else:
		return max(similarities)

#LSI word similarity calculation
def getLSISimilarity(dictionary, US, word1, word2):
	index1 = getIndexInDictionary(dictionary, word1)
	index2 = getIndexInDictionary(dictionary, word2)

	vector1 = gensim.matutils.unitvec(US[index1, :])
	vector2 = gensim.matutils.unitvec(US[index2, :])

	return np.dot(vector1, vector2)

def cleanDoc(doc, stopset, stemmer):
	f = open(doc, 'r')
	tokens = []
	for line in f:
	    tokens.append(word_tokenize(line))
	f.close()

	clean = [eachtoken for token in tokens for eachtoken in token if eachtoken.lower() not in stopset and len(eachtoken) > 2]
	final = ""
	for word in clean:
		final += stemmer.stem(word) +" "
	return final

def createCorpus():
	files = os.listdir(textFileLocation)
	allContent = ""
	for textFile in files:
#		cleanedDoc = cleanDoc(textFileLocation+textFile, stopset, stemmer)
#		cleanedDoc = unicode(cleanedDoc, errors='ignore') #had to do this since it gave some error in encoding
#		allContent += cleanedDoc+'\n'
		allContent += open(textFileLocation+textFile, 'r').read() +"\n"
	f = open('corpus.txt', 'w')
	f.write(allContent)

class MyCorpus(object):
    def __iter__(self):
	dictionary = corpora.Dictionary(line.lower().split() for line in open('corpus.txt'))
	for line in open('corpus.txt'):
            	yield dictionary.doc2bow(line.lower().split())

def getIndexInDictionary(dictionary, word):
	index = -1
	try:
		index = list(dictionary.keys())[list(dictionary.values()).index(word.lower())] #given a word, return the index position of the word in the dictionary
	except ValueError:
		index = -1
	return index

#the POS tags return by nltk and wordnet is different in syntax, so here is the mapping
def get_wordnet_pos(treebank_tag):
    #if treebank_tag.startswith('J'):
     #   return wn.ADJ
    if treebank_tag.startswith('V'):
        return wn.VERB
    elif treebank_tag.startswith('N'):
        return wn.NOUN
    elif treebank_tag.startswith('R'):
        return wn.ADV
    else:
        return ''



def init():
 	#create the corpus
        createCorpus() #do once
        corpus = MyCorpus() #do once
        corpora.MmCorpus.serialize('corpus.mm', corpus) #do once
	dictionary = corpora.Dictionary(line.lower().split() for line in open('corpus.txt'))
	#create the tfidf model from the corpus


def getSimilarity(sentence1, sentence2, dictionary, corpus, tfidf, US):

	tokens1 = word_tokenize(sentence1)
	tokens2 = word_tokenize(sentence2)
#	tokens1 = [w for w in tokens1 if not w in stopset]
#	tokens2 = [w for w in tokens2 if not w in stopset]
	pos_tag1 = nltk.pos_tag(tokens1)
        pos_tag2 = nltk.pos_tag(tokens2)
	
	#implementation of similaroty measuring equation in Mihalcea[2006] 
	first_term = 0
	second_term = 0
	first_idf_total = 0
	second_idf_total = 0

	for pos1 in pos_tag1:
		max_sim1 = 0
		pos1 = list(pos1)
		pos1[1] = get_wordnet_pos(pos1[1])
		for pos2 in pos_tag2:
			pos2 = list(pos2)
			pos2[1] = get_wordnet_pos(pos2[1])
			if pos1[1]==pos2[1] and (pos1[1]==wn.VERB or pos1[1]==wn.NOUN):
				sim1 = getKnowledgeBasedSimilarity(pos1, pos2)
				if sim1 > max_sim1:
					max_sim1 = sim1
				lsi_sim1 = getLSISimilarity(dictionary, US, pos1[0], pos2[0])
#				lsi_sim1 = getLSISimilarity(dictionary, US, stemmer.stem(pos1[0]), stemmer.stem(pos2[0]))
				max_sim1 = max(max_sim1, lsi_sim1)
		
		if max_sim1 > 0:
			stemmed_term1 = pos1[0]#stemmer.stem(pos1[0])
			index = getIndexInDictionary(dictionary, stemmed_term1)
			idf1 = 0
			if index != -1:
				idf1 = tfidf.idfs[index]
			else:
				idf1 = 0
			first_term += max_sim1*idf1
			first_idf_total += idf1

	for pos2 in pos_tag2:
                max_sim2 = 0
		pos2 = list(pos2)
                pos2[1] = get_wordnet_pos(pos2[1])
                for pos1 in pos_tag1:
			pos1 = list(pos1)
                        pos1[1] = get_wordnet_pos(pos1[1])
                        if pos2[1]==pos1[1] and (pos1[1]==wn.VERB or pos1[1]==wn.NOUN):
                                sim2 = getKnowledgeBasedSimilarity(pos2, pos1)
                                if sim2 > max_sim2:
                                        max_sim2 = sim2
				lsi_sim2 = getLSISimilarity(dictionary, US, pos2[0], pos1[0])
#				lsi_sim2 = getLSISimilarity(dictionary, US, stemmer.stem(pos2[0]), stemmer.stem(pos1[0]))
                        	max_sim2 = max(max_sim2, lsi_sim2)

		if max_sim2 > 0:
	                stemmed_term2 = pos2[0]#stemmer.stem(pos2[0])
			index = getIndexInDictionary(dictionary, stemmed_term2)
                        idf2 = 0
                        if index != -1:
                                idf2 = tfidf.idfs[index]
                        else:
                                idf2 = 0
                	second_term += max_sim2*idf2
	                second_idf_total += idf2

	similarity = 0
	if first_idf_total == 0 or second_idf_total == 0:
		print sentence1 +" : "+sentence2+"\n"
		similarity = -1
	else:	
		similarity = (first_term/first_idf_total + second_term/second_idf_total)/2
	return similarity



if __name__ == "__main__":
#	init()
	dictionary = corpora.Dictionary(line.lower().split() for line in open('corpus.txt'))
        #create the tfidf model from the corpus
        corpus = corpora.MmCorpus('corpus.mm')
        tfidf = models.TfidfModel(corpus)
        lsi = gensim.models.lsimodel.LsiModel(corpus=corpus, id2word=dictionary, num_topics=400)
        U = lsi.projection.u
        S = lsi.projection.s
        US = np.multiply(U, S)
	
	data_folder='conditions-test'
	files = glob.glob('../data-preparation/'+data_folder+'/*-candidates')

	#read definitions
	for f in files:
		cui=f[f.rindex('/')+1:f.rindex('-')]
	        definitions = open('../data-preparation/def-files/'+cui+'-definitions').readlines()
	        candidates = open('../data-preparation/conditions-originals/'+cui).readlines()
		results = ''
		allSimilarities = []
		for candidate in candidates:
			comps = candidate.split('\t')
			candidate = comps[0]
			similarity = []
			for defn in definitions:
				defn = defn.replace('\n', '')
				simVal = getSimilarity(candidate, defn, dictionary, corpus, tfidf, US)
				similarity.append(simVal)
			allSimilarities.append(max(similarity))
			results += str(max(similarity))+"\n"
		f = open('../data-preparation/mihalcea-results/'+cui,'w')
	        f.write(results)
       	f.close()
