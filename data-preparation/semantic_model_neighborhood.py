from nltk import sent_tokenize
from nltk.corpus import stopwords
from corenlp import StanfordCoreNLP
from sets import Set
import os

corenlp_dir="stanford-corenlp-full-2014-08-27/"
corenlp=StanfordCoreNLP(corenlp_dir)
stopset=set(stopwords.words('english'))

valid_pos=['JJ', 'JJR', 'JJS', 'NN', 'NNS', 'NNPS', 'RB', 'RBR', 'RBS', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
def getNeighborhood(sentence, keyword, length):
        parsed=corenlp.raw_parse(sentence)
        words=parsed['sentences'][0]['words']
        word_list=[]
        lst=[]
        key_indices=[]
        index=-1
        for word in words:
		index+=1
                lexical=word[0]
                pos=word[1]['PartOfSpeech']
                word_list.append((lexical, pos))
                if lexical==keyword:
                        key_indices.append(index)
                
	for key_index in key_indices:
		s=''
	        for i in range(key_index, key_index+length):
        	        if i<len(word_list) and word_list[i][1] in valid_pos and word_list[i][0] not in stopset:
                	        s+=word_list[i][0]+', ' #lst.append(word_list[i][0])
	        for i in range(key_index-length, key_index):
        	        if i>-1 and word_list[i][1] in valid_pos and word_list[i][0] not in stopset:
                	        s+=word_list[i][0]+', ' #lst.append(word_list[i][0])
		lst.append(s)
        return lst



conditions=open('keywords-for-condition').readlines()
for condition in conditions:
        cui=condition.split('\t')[0]
	keyword=condition.split('\t')[2].strip()
        models=''
       	definitions=open('def-files/'+cui+"-definitions").readlines()
        for definition in definitions:
                definition=sent_tokenize(definition)[0]
		if keyword in definition:
	                prunedSentences=getNeighborhood(definition, keyword, 4)
		else:
			continue
		s=''	
                for e in prunedSentences:
			models+=e+'\n' #s+=e+','
	#	models+=s+'\n'
        f=open("semantic-models-raw/"+cui, 'w')
        f.write(models)
        f.close()

#remove duplicates
files=os.listdir("semantic-models-raw")
for f in files:
	semantic_models=open('semantic-models-raw/'+f).readlines()
	allmodels=Set()
	for model in semantic_models:
		modelset=Set()
		for term in model.strip()[:-1].split(','):
			modelset.add(term.strip())
		allmodels.add(modelset)

	uniquemodels=''
	for model in allmodels:
		s=''
		for word in model:
			s+=word+','
		uniquemodels+=s+'\n'
		
	f=open("semantic-models-raw/"+f, 'w')
	f.write(uniquemodels)
	f.close()
