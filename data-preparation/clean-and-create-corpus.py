import nltk
from nltk.tokenize import RegexpTokenizer

stemmer=nltk.PorterStemmer()
tokenizer=RegexpTokenizer(r'\w+')
annotated_sentences=open('annotated-sentences/annotated-sentences').readlines()
keywords=open('keywords-for-condition').readlines()

keywords_for_condition=dict()
for line in keywords:
	keywords_for_condition[line.split('\t')[0]]=[line.split('\t')[1].strip()]
lst=keywords_for_condition['C0013404']
lst.append('ventil')
lst.append('respir')
keywords_for_condition['C0013404']=lst

content=''
for annotated_sentence in annotated_sentences:
	components=annotated_sentence.split('\t')
	sentence=components[0]
	cui=components[4]
	keywords=keywords_for_condition[cui.strip()]
	stem_and_token=dict()
	stemmed=[]
	
	for token in tokenizer.tokenize(components[0]):
		stemmed_token=stemmer.stem(token)
		stemmed.append(stemmed_token)
		stem_and_token[stemmed_token]=token
	
	common=[w1 for w1 in keywords if w1 in stemmed]
	if len(common)>0:
		content+=annotated_sentence.strip()+"\t"+str(stem_and_token[common[0]])+"\n"
#	else:
#		print annotated_sentence
print content
