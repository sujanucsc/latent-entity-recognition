import os
from nltk.tokenize import RegexpTokenizer
import nltk
from nltk.corpus import stopwords
from collections import OrderedDict
from nltk.tokenize import sent_tokenize
from textblob import TextBlob
from corenlp import StanfordCoreNLP
from collections import Counter
from sets import Set

tokenizer=RegexpTokenizer(r'\w+')
stemmer=nltk.PorterStemmer()
stopset=set(stopwords.words('english'))
corenlp_dir = "stanford-corenlp-full-2014-08-27/"
corenlp = StanfordCoreNLP(corenlp_dir)
token_ordered_file='entropy-stemmed'

def getSemanticModel(dependency_tree, keyword):
	semantic_model=[]
	heads_and_modifiers=dict()

	#fill the heads_and_modifiers : heads_and_modifiers contains the mappind words to the given keyword in the dependency tree and their modifiers
	for dependency in dependency_tree:
		head=dependency[1].split('-')[0]
		tail=dependency[2].split('-')[0]
		if keyword==stemmer.stem(head) and (dependency[0]=='amod' or dependency[0]=='nn'):
			if heads_and_modifiers.has_key(dependency[1]):
				items=heads_and_modifiers[dependency[1]]
				items.append(tail)
			else:
				items=[tail]
				heads_and_modifiers[dependency[1]]=items	
		if keyword==stemmer.stem(tail):
			if not heads_and_modifiers.has_key(dependency[2]):
				heads_and_modifiers[dependency[2]]=[]
	
	for head in heads_and_modifiers.keys():
		path=[]
		path.append(head)
		paths=getPaths(dependency_tree, head, path)
		paths=addModifiersOnPath(dependency_tree, paths, head)
		#once we have the path, we will create multiple paths from that by appending modifiers of the keyword stored in the heads_and_modifiers
		#if there are dependencies of (amod, breathing, x), we will add new path with x to the path set
		if len(heads_and_modifiers[head])>0:
			for modifier in heads_and_modifiers[head]:
				temp=paths
				temp.append(modifier)
				semantic_model.append(temp)			
		else:
			semantic_model.append(paths)

	return semantic_model

#once we find the path, we will add the terms associated with 'amod' relationships to the terms on the path
def addModifiersOnPath(dependency_tree, path, head):
	for dependency in dependency_tree:
		if dependency[1]!=head and dependency[1] in path and dependency[0]=='amod':
			path.append(dependency[2])
	return path

#find the path from 'head' to the 'ROOT' of the given dependency tree 
def getPaths(dependency_tree, head, path):
        if 'ROOT' in head:
		del path[-1]#remove added 'ROOT' element to the path, it is always the last element
                return path
        for dependency in dependency_tree:
                if head==dependency[2]:
                        head=dependency[1]
                        path.append(head)
                        return getPaths(dependency_tree, head, path)

if __name__ == "__main__":
	conditions=open('keywords-for-condition').readlines()
	for condition in conditions:
		cui=condition.split('\t')[0]
		all_models=''
		definitions=open('def-files/'+cui+"-definitions").readlines()
		for definition in definitions:
			definition=sent_tokenize(definition)[0]
			processed_info=corenlp.raw_parse(definition)
			dependency_tree=processed_info['sentences'][0]['indexeddependencies']
			sem_models=getSemanticModel(dependency_tree, condition.split('\t')[1].strip())
			for model in sem_models:
				for term in model:
					all_models+=str(term.split('-')[0])+","
				all_models+="\n"
		f=open("semantic-models-dependency/"+cui, 'w')
		f.write(all_models)
		f.close()
	

#remove duplicates
files=os.listdir("semantic-models-dependency")
for f in files:
        semantic_models=open('semantic-models-dependency/'+f).readlines()
        allmodels=Set()
        for model in semantic_models:
                modelset=Set()
                for term in model.strip()[:-1].split(','):
                        modelset.add(term)
                allmodels.add(modelset)

        uniquemodels=''
        for model in allmodels:
                s=''
                for word in model:
                        s+=word+','
                uniquemodels+=s+'\n'

        f=open("semantic-models-dependency/"+f, 'w')
        f.write(uniquemodels)
        f.close()
	
