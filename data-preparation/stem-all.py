from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
import nltk
import os
from nltk.tokenize import RegexpTokenizer

tokenizer = RegexpTokenizer(r'\w+')

textFileLocation = 'all-definitions/'
stopset = set(stopwords.words('english'))
stemmer = nltk.PorterStemmer()

def cleanDoc(doc, stopset, stemmer):
        f = open(doc, 'r')
        tokens = []
        for line in f:
            tokens.append(tokenizer.tokenize(line))
        f.close()

        clean = [eachtoken for token in tokens for eachtoken in token if eachtoken.lower() not in stopset]
        final = ""
        for word in clean:
                final += stemmer.stem(word) +" "
        return final

files = os.listdir(textFileLocation)
allContent = ""
for textFile in files:
	cleanedDoc = cleanDoc(textFileLocation+textFile, stopset, stemmer)
	cleanedDoc = unicode(cleanedDoc, errors="ignore")
	f = open('stemmed-def-files/'+textFile, 'w')
	f.write(cleanedDoc)
	f.close()

