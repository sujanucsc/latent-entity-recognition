import os
from nltk.tokenize import RegexpTokenizer
import nltk
from nltk.corpus import stopwords
from collections import OrderedDict
from nltk.tokenize import sent_tokenize
from textblob import TextBlob
from corenlp import StanfordCoreNLP
from collections import Counter

tokenizer=RegexpTokenizer(r'\w+')
stemmer=nltk.PorterStemmer()
stopset=set(stopwords.words('english'))
corenlp_dir = "stanford-corenlp-full-2014-08-27/"
corenlp = StanfordCoreNLP(corenlp_dir)
token_ordered_file='entropy-stemmed'


def getPOS(sentence):
        word_with_pos=[]
        parsed_sentence=corenlp.raw_parse(sentence)
        for element in parsed_sentence['sentences'][0]['words']:
                word=element[0]
                pos=element[1]['PartOfSpeech']
                word_with_pos.append((word, pos))

        return word_with_pos


tokens_and_values=dict()
valid_pos=['NN', 'NNS', 'NNP', 'NNPS']
for line in open(token_ordered_file).readlines():
	tokens_and_values[line.split('\t')[0]]=line.split('\t')[1].strip()

for f in os.listdir('all-definitions/'):
	print f
	definitions=open('all-definitions/'+f).readlines()
	all_stemmed_tokens=[]
	top_words=[]
	for definition in definitions:
		definition=sent_tokenize(definition)[0] 
		all_stemmed_tokens+=[stemmer.stem(token) for token in tokenizer.tokenize(definition)]

	stemmed_and_word=dict()
	for definition in definitions:
		definition=sent_tokenize(definition)[0]
		tagged=getPOS(definition)#TextBlob(definition)
		tokens=[tag[0] for tag in tagged if tag[1] in valid_pos and tag[0] not in stopset]
		stemmed_with_values=dict()
		stemmed=[]
		for token in tokens:
			stemmed_token=stemmer.stem(token)
			stemmed.append(stemmed_token)
			stemmed_and_word[stemmed_token]=token
		for word in stemmed:
			try:	
				stemmed_with_values[word]=float(tokens_and_values[word])*float(all_stemmed_tokens.count(word))
			except KeyError:
				stemmed_with_values[word]=0
		sorted_dict=OrderedDict(sorted(stemmed_with_values.items(), key=lambda t: float(t[1])))
		if len(sorted_dict) !=0:
			top_words.append(list(reversed(sorted_dict))[0])
	print f.split('-')[0]+"\t"+Counter(top_words).most_common(1)[0][0]+"\t"+stemmed_and_word[str(Counter(top_words).most_common(1)[0][0])]
