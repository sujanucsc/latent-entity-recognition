import semantic_model_dependency as sm
from corenlp import StanfordCoreNLP
from nltk.tokenize import RegexpTokenizer
from nltk.corpus import stopwords
import nltk

corenlp_dir="stanford-corenlp-full-2014-08-27/"
corenlp=StanfordCoreNLP(corenlp_dir)
tokenizer=RegexpTokenizer(r'\w+')
stemmer=nltk.PorterStemmer()
stopset=set(stopwords.words('english'))

def getPrunedSentence(sentence, keyword):
        processed_info=corenlp.raw_parse(sentence)
        dependency_tree=processed_info['sentences'][0]['indexeddependencies']
        sem_models=sm.getSemanticModel(dependency_tree, keyword)
	
	models=[]
        for model in sem_models:
		all_models=''
                for term in model:
                        all_models+=str(term.split('-')[0])+","
                models.append(all_models)
	return models

valid_pos=['JJ', 'JJR', 'JJS', 'NN', 'NNS', 'NNPS', 'RB', 'RBR', 'RBS', 'VB', 'VBD', 'VBG', 'VBN', 'VBP', 'VBZ']
def getNeighborhood(sentence, keyword, length):
	sentence=sentence.split('\t')[0]
	parsed=corenlp.raw_parse(sentence)
	found=0
	#the parse tree may identifies multiple sentences, so we need to identify the sentence with the keyword and get the neighborhood of that
	for sentence in parsed['sentences']:
		if found:
			break
		for word in sentence['words']:
			if keyword in word[0]:
				words=sentence['words']
				found=1
				break

	word_list=[]
	lst=[]
	key_indices=[]
	index=0
	for word in words:
		lexical=word[0]
		pos=word[1]['PartOfSpeech']
		word_list.append((lexical, pos))
		if lexical==keyword:
			key_indices.append(index)
		index+=1

	for key_index in key_indices:
		s=''
		for i in range(key_index, key_index+length):
			if i<len(word_list) and word_list[i][1] in valid_pos and word_list[i][0] not in stopset:
				s+=word_list[i][0]+', ' #lst.append(word_list[i][0])
		for i in range(key_index-length, key_index):
			if i>-1 and word_list[i][1] in valid_pos and word_list[i][0] not in stopset:
				s+=word_list[i][0]+', ' #lst.append(word_list[i][0])
		lst.append(s)
	return lst

sentences=open('annotated-sentences/annotated-sentences').readlines()

for sentence in sentences:
	content=''
	components=sentence.split('\t')
	cui=components[4].strip()
	semantic_models=open('semantic-models-original/'+cui).readlines()
	
	#the relevant portion of the candidate sentence is selected based on the dependancy tree
#	prunedSentences=getPrunedSentence(components[0], stemmer.stem(components[5].strip()))
#	for prunedSentence in prunedSentences:
#		for semantic_model in semantic_models:
#			content+=semantic_model.strip()+':'+prunedSentence+'\n'

	#the whole candidate sentence is compared with the semantic model
	tokens=tokenizer.tokenize(components[0])
	s=''
	for token in tokens:
		s+=token+','
	for semantic_model in semantic_models:
	       	content+=semantic_model.strip()+':'+s+'\n'
	
	#we select adjectives and adverbs which appear in the neighborhood of the domain related term in the candidate sentence
#	prunedSentences=getNeighborhood(sentence, components[5].strip(), 3)
#	for e in prunedSentences:
#		for semantic_model in semantic_models:
#			content+=semantic_model.strip()+':'+e+'\n' 	

	content+='\n'
	f=open('prepared_data/def_original/conditions-original/'+cui+'-candidates', 'a+')
	f.write(content)
	f.close()

	f=open('prepared_data/def_original/conditions-original/'+cui+'-gold', 'a+')
	f.write(components[1]+'\t'+components[2]+'\n')
	f.close()


