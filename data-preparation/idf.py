from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords
from gensim import corpora, models
import gensim
import nltk
import os
from nltk.tokenize import RegexpTokenizer

textFileLocation = 'stemmed-data-files/'
stopset = set(stopwords.words('english'))
stemmer = nltk.PorterStemmer()
tokenizer=RegexpTokenizer(r'\w+')

class MyCorpus(object):
    def __iter__(self):
        dictionary = corpora.Dictionary(line.lower().split() for line in open('corpus.txt'))
        for line in open('corpus.txt'):
                yield dictionary.doc2bow(line.lower().split())

def createCorpus():
        files = os.listdir(textFileLocation)
        allContent = ""
        for textFile in files:
               allContent += open(textFileLocation+textFile).read()+"\n"
        f = open('corpus.txt', 'w')
        f.write(allContent)

def init():
        #create the corpus
        createCorpus() #do once
        corpus = MyCorpus() #do once
        corpora.MmCorpus.serialize('corpus.mm', corpus) #do once
        #create the tfidf model from the corpus

def getIndexInDictionary(dictionary, word):
        index = -1
        try:
                index = list(dictionary.keys())[list(dictionary.values()).index(word.lower())] #given a word, return the index position of the word in the dictionary
        except ValueError:
                index = -1
        return index

idfstr=''
if __name__ == "__main__":
        init()
        dictionary = corpora.Dictionary(line.lower().split() for line in open('corpus.txt'))
        #create the tfidf model from the corpus
        corpus = corpora.MmCorpus('corpus.mm')
        tfidf = models.TfidfModel(corpus)

	files = os.listdir(textFileLocation)
	tokens = set()
        for f in files:
	        for line in open(textFileLocation+f):
        		[tokens.add(token) for token in tokenizer.tokenize(line)]
	
	for token in tokens:
		index = getIndexInDictionary(dictionary, token)
		idf = 0
                if index != -1:
                	idfstr+=token+"\t"+str(tfidf.idfs[index])+"\n"
#			print token +"\t"+ str(idf)

f=open('idf-stemmed', 'w')
f.write(idfstr)
f.close()

		
#	print tokens

	
