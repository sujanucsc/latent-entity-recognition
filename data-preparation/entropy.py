import os
import re
import collections
import math

textFileLocation='stemmed-def-files/'

tokenSet=set()
files = os.listdir(textFileLocation)
for f in files:
	tokens = open(textFileLocation+f).read().split()
	for token in tokens:
		tokenSet.add(token)

specificityStr=''
for token in tokenSet:
	tokenCount=[]
	denominator=0.0
	for f in files:
		word_list = open(textFileLocation+f).read().split()
		tokenCount.append(word_list.count(token))
	total=sum(tokenCount)
	
	for count in tokenCount:
		probability=count/float(total)
		if probability!=0:
			denominator+=probability*math.log(probability, 2)
	
	specificity = 1/((-denominator)+0.001)
	specificityStr+=token+"\t"+str(specificity)+"\n"


f=open('entropy-stemmed','w')
f.write(specificityStr)
f.close()

