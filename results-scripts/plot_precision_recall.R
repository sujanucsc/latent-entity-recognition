setwd(".")

args <- commandArgs(trailingOnly = TRUE)
filename <- args[1]
cat("Reading from file:",filename,"\n")
# load evaldata from csv into variable d
d <- read.csv(filename, sep="\t", header=TRUE)
# add a new column with the f1
#d$f1 <- 2* d$precision * d$recall / (d$precision + d$recall)

# selects rows with confidence 3, and all columns
#conf3 <- d[d$confidence=="1",]
#conf3 <- d
# selects only rows for a given method
#sujan <- conf3[d$method=="OUR",]
#mihalcea <- conf3[d$method=="Mihalcea",]
#syntac <- conf3[d$method=="Levenshtein",]
# starts an empty plot
#setEPS()
#postscript(paste(filename,".eps", sep=""))
pdf(paste(filename,".pdf",sep=""))
par(mfrow=c(2,2),oma=c(1,0,0,0),xpd=NA)

conf1 <- d[d$confidence=="1",]
# selects only rows for a given method
sujan <- conf1[d$method=="OUR",]
mihalcea <- conf1[d$method=="Mihalcea",]
#syntac <- conf1[d$method=="Levenshtein",]

plot(c(0,1),c(0,1),xlab="recall",ylab="precision",main="Confidence > 1",type="n")
lines(sujan$pos_recall, sujan$pos_precision,type="b",pch=1,col="blue")
#text(sujan$recall, sujan$precision)

lines(mihalcea$pos_recall, mihalcea$pos_precision, type="b",pch=2,col="red")
#text(mihalcea$recall, mihalcea$precision,  cex=0.5,pos=4)

#lines(syntac$recall, syntac$precision, type="b",pch=3,col="green")
#text(syntac$recall, syntac$precision, cex=0.5,pos=4)

conf2 <- d[d$confidence=="2",]
# selects only rows for a given method
sujan <- conf2[d$method=="OUR",]
mihalcea <- conf2[d$method=="Mihalcea",]
#syntac <- conf2[d$method=="Levenshtein",]

plot(c(0,1),c(0,1),xlab="recall",ylab="precision",main="Confidence > 2",type="n")
lines(sujan$pos_recall, sujan$pos_precision,type="b",pch=1,col="blue")
#text(sujan$recall, sujan$precision, cex=0.5,pos=4)

lines(mihalcea$pos_recall, mihalcea$pos_precision, type="b",pch=2,col="red")
#text(mihalcea$recall, mihalcea$precision, cex=0.5,pos=4)

#lines(syntac$recall, syntac$precision, type="b",pch=3,col="green")
#text(syntac$recall, syntac$precision, cex=0.5,pos=4)

conf3 <- d[d$confidence=="3",]
# selects only rows for a given method
sujan <- conf3[d$method=="OUR",]
mihalcea <- conf3[d$method=="Mihalcea",]
#syntac <- conf3[d$method=="Levenshtein",]

#eps(paste(filename,".pdf",sep=""));
plot(c(0,1),c(0,1),xlab="recall",ylab="precision",main="Confidence > 3",type="n")
# add a line with recall and precision values for each threshold for sujan's method
lines(sujan$pos_recall, sujan$pos_precision,  # x,y points for the line
      type="b",                       # type of line
      pch=1,                          # type of character to mark points in the line
      col="blue")                     # color
#text(sujan$recall, sujan$precision, cex=0.5, pos=4)
# add a line with recall and precision values for each threshold for mihalcea's method
lines(mihalcea$pos_recall, mihalcea$pos_precision, type="b",pch=2,col="red")
#text(mihalcea$recall, mihalcea$precision, cex=0.5,pos=4)

#lines(syntac$recall, syntac$precision, type="b",pch=3,col="green")
#text(syntac$recall, syntac$precision, cex=0.5,pos=4)
#TODO can have an array with method names, array for colors, point chars, etc. 
#     and use a for loop to plot all of them instead of repeating code.

#plot(sujan$confidence,sujan$f1,type="b",pch=1,col="blue")
#lines(mihalcea$confidence,mihalcea$f1,type="b",pch=2,col="red")

conf4 <- d[d$confidence=="4",]
# selects only rows for a given method
sujan <- conf4[d$method=="OUR",]
mihalcea <- conf4[d$method=="Mihalcea",]
#syntac <- conf4[d$method=="Levenshtein",]

plot(c(0,1),c(0,1),xlab="recall",ylab="precision",main="Confidence > 4",type="n")
lines(sujan$pos_recall, sujan$pos_precision,type="b",pch=1,col="blue") 
#text(sujan$recall, sujan$precision,cex=0.5,pos=4)

lines(mihalcea$pos_recall, mihalcea$pos_precision, type="b",pch=2,col="red")
#text(mihalcea$recall, mihalcea$precision, cex=0.5,pos=4)

#lines(syntac$recall, syntac$precision, type="b",pch=3,col="green")
#text(syntac$recall, syntac$precision, cex=0.5,pos=4)

#legend(-1, -0.4,pch = c(1,2,3), ncol=3,c("OUR","Mihalcea","Levenshtein"), 
 #   col=c("blue","red","green"), bty="n")
dev.off()
