import glob
import sys

thresholds=[-0.5, -0.4, -0.3, -0.2, -0.1, 0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
#thresholds=[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]
confidence_vals=[1, 2, 3, 4]
results_folder='../data-preparation/fernando-results-our-format'
files = glob.glob(results_folder+'/*-results')
lineNumber = 0
	
for confidencethreshold in confidence_vals:
	for threshold in thresholds:
		allpositivecorrect = 0
		allpositiveincorrect = 0
		allnegativecorrect = 0
		allnegativeincorrect = 0
		for f in files:
		        positivecorrect=0
		        positiveincorrect=0
		        negativecorrect=0
		        negativeincorrect=0

		        cui=f[f.rindex('/')+1:f.rindex('-')]
#		        cui=cui[:cui.rindex('-')]
		        results=open(f).readlines()
		        sentences=open('data-preparation/prepared_data/def_dependency/conditions-originals/'+cui).readlines()
		        lineNumber=0
			for result in results:
		                wordnet_sim = float(result.split(':')[0].strip())
       				sentence_comps=sentences[lineNumber].split('\t')
                		annotation=sentence_comps[1]
                		confidence=float(sentence_comps[2])
					
				if confidence > confidencethreshold:# and related.lower()=='yes':
                               		if annotation.lower()=='positive':
                                                if wordnet_sim >= threshold:
       		                                        positivecorrect +=1
                       		                else:
                                       		        positiveincorrect +=1
	                                elif annotation.lower()=='negative' or annotation.lower()=='neutral':
        	                                if wordnet_sim < threshold:
                       		                        negativecorrect +=1
                                       		else:
	                                                negativeincorrect +=1
				lineNumber += 1

			allpositivecorrect+=positivecorrect
		        allpositiveincorrect+=positiveincorrect
        		allnegativecorrect+=negativecorrect
        		allnegativeincorrect+=negativeincorrect

		accuracy=float(allpositivecorrect + allnegativecorrect) / (allpositivecorrect+allpositiveincorrect+allnegativecorrect+allnegativeincorrect)
		positive_precision=float(allpositivecorrect)/(allpositivecorrect+allnegativeincorrect)
		positive_recall=float(allpositivecorrect)/(allpositivecorrect+allpositiveincorrect)
		try:
			nonpositive_precision=float(allnegativecorrect)/(allnegativecorrect+allpositiveincorrect)
		except ZeroDivisionError:
			nonpositive_precision=0
		nonpositive_recall=float(allnegativecorrect)/(allnegativecorrect+allnegativeincorrect)
		positiveF1=(2*positive_precision*positive_recall)/(positive_precision+positive_recall)
		try:
			nonpositiveF1=(2*nonpositive_precision*nonpositive_recall)/(nonpositive_precision+nonpositive_recall)
		except ZeroDivisionError:
                        nonpositiveF1=0
			
		print str(confidencethreshold)+"\t"+str(threshold)+"\t"+str(accuracy)+"\t"+str(positive_precision)+"\t"+str(positive_recall)+"\t"+str(positiveF1)+"\t"+str(nonpositive_precision)+"\t"+str(nonpositive_recall)+"\t"+str(nonpositiveF1)+"\t"+"Fernando"
