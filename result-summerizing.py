import glob
import sys


results_folder='data-preparation/fernando-results-our-format'
files = glob.glob(results_folder+'/*-results')
allpositivecorrect = 0
allpositiveincorrect = 0
allnegativecorrect = 0
allnegativeincorrect = 0
threshold = float(sys.argv[1])
confidencethreshold=float(sys.argv[2])
lineNumber = 0

for f in files:
	positivecorrect=0
	positiveincorrect=0
	negativecorrect=0
	negativeincorrect=0

	cui=f[f.rindex('/')+1:f.rindex('-')]
#	cui=cui[:cui.rindex('-')]
	results=open(f).readlines()
	sentences=open('data-preparation/prepared_data/def_dependency/conditions-originals/'+cui).readlines()
	lineNumber=0
	for result in results:
		wordnet_sim = float(result.split(':')[0].strip())
		sentence_comps=sentences[lineNumber].split('\t')
		annotation=sentence_comps[1]
		confidence=float(sentence_comps[2])
		
		if confidence > confidencethreshold:# and related.lower()=='yes':
                	if annotation.lower()=='positive':
                        	if wordnet_sim >= threshold:
                                	positivecorrect +=1
                        	else:
                                	positiveincorrect +=1
#                               	print sentence
                	elif annotation.lower()=='negative' or annotation.lower()=='neutral':
                        	if wordnet_sim < threshold:
                                	negativecorrect +=1
                        	else:
                                	negativeincorrect +=1
#                               print sentence + str(wordnet_sim)
#                	elif annotation.lower()=='neutral':
#                        	if wordnet_sim < threshold and wordnet_sim > -5:
#                                	neutralcorrect +=1
#                        	else:
#                                	neutralincorrect +=1
        	lineNumber += 1
	print cui, positivecorrect, positiveincorrect, negativecorrect, negativeincorrect 
	if (positivecorrect+positiveincorrect+negativecorrect+negativeincorrect) == 0:
		print 0
	else:
		print float(positivecorrect + negativecorrect) / (positivecorrect+positiveincorrect+negativecorrect+negativeincorrect)
	allpositivecorrect+=positivecorrect
	allpositiveincorrect+=positiveincorrect
	allnegativecorrect+=negativecorrect
	allnegativeincorrect+=negativeincorrect

print float(allpositivecorrect + allnegativecorrect) / (allpositivecorrect+allpositiveincorrect+allnegativecorrect+allnegativeincorrect)
